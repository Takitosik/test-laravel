<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mockery\Exception;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * App\Role relation.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public static function checkAccess($permission)
    {
        $p = Permission::where('name', '=', $permission)->first();;
        if (NULL == $p || !isset($p->roles[0]->pivot->role_id))
            return false;

        foreach (auth()->user()->roles as $role){
            if ($role->id == $p->roles[0]->pivot->role_id)
                return true;
        }

    }
}
