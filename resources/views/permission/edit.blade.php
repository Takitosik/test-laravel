@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif

            {!! Form::select('user_id', $users, null, ['class' => 'form-control']) !!}
            {!! Form::select('role_id', $role, null, ['class' => 'form-control']) !!}

        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>Stock Name</td>
                <td colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div>
@endsection